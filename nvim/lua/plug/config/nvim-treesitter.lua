require("nvim-treesitter.configs").setup {
	ensure_installed = { "bash", "c", "lua", "markdown", },
	highlight = {
		enable = true,
	},
}
