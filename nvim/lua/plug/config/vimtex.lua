-- External programs
vim.api.nvim_set_var("vimtex_view_method", "zathura")
vim.api.nvim_set_var("tex_flavor", "latex")
vim.api.nvim_set_var("vimtex_compiler_latexmk", { options = { "-shell-escape" } })

-- Enable Vimtex syntax conceal
vim.opt.conceallevel = 2
vim.opt.concealcursor = "n"

-- Folding
vim.api.nvim_create_autocmd(
	"FileType", {
		pattern = "tex",
		callback = function()
			vim.api.nvim_set_var("vimtex_fold_enabled", 1)
		end
	}
)
vim.api.nvim_set_var("vimtex_fold_types", {
	comments = {
		enabled = 0
	},
	envs = {
		blacklist = {
			"table"
		}
	},
	env_options = {
		enabled = 1,
	},
	preamble = {
		enabled = 1
	},
	sections = {
		enabled = 1,
		sections = {
			"appendix",
			"chapter",
			"section",
			"subsection",
			"subsubsection",
			"question",
			"subquestion",
		},
	}
})
