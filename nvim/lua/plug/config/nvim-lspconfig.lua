local capabilities = require("cmp_nvim_lsp").default_capabilities()
local lspconfig = require("lspconfig")

lspconfig.bashls.setup{
	capabilities = capabilities
}
lspconfig.ccls.setup{
	capabilities = capabilities
}
lspconfig.ltex.setup{
	autostart = false,
	capabilities = capabilities
}
lspconfig.lua_ls.setup{
	capabilities = capabilities
}
