-- Bootstrap plugin manager
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

-- Plugins
require("lazy").setup({
	{ "hrsh7th/nvim-cmp",
		config = function() require("plug.config.nvim-cmp") end,
		dependencies = {
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-cmdline",
			"hrsh7th/cmp-nvim-lsp",
			"quangnguyen30192/cmp-nvim-ultisnips",
			"jc-doyle/cmp-pandoc-references",
			"hrsh7th/cmp-path",
			"onsails/lspkind.nvim"
		}
	},
	{ "neovim/nvim-lspconfig",
		config = function() require("plug.config.nvim-lspconfig") end
	},
	{ "nvim-tree/nvim-tree.lua",
		config = function() require("plug.config.nvim-tree") end,
		dependencies = { "nvim-tree/nvim-web-devicons" }
	},
	{ "nvim-treesitter/nvim-treesitter",
		build = function() vim.cmd("TSUpdate") end,
		config = function() require("plug.config.nvim-treesitter") end
	},
	"unblevable/quick-scope",
	"SirVer/ultisnips",
	"tpope/vim-commentary",
	"psliwka/vim-smoothie",
	{ "tpope/vim-surround",
		dependencies = { "tpope/vim-repeat" }
	},
	{ "dhruvasagar/vim-table-mode",
		config = function() require("plug.config.vim-table-mode") end
	},
	"lukesmithxyz/vimling",
	{ "lervag/vimtex",
		config = function() require("plug.config.vimtex") end
	}
})
