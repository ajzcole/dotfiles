-- Commands to take place after buffer filetype is set
vim.api.nvim_create_autocmd(
	"FileType", {
		pattern = "tex",
		callback = function()
			vim.opt.linebreak = true
		end
	}
)
