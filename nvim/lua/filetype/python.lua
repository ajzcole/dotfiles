-- Prevent tabs from being changed to spaces
vim.api.nvim_set_var("python_recommended_style", 0)
