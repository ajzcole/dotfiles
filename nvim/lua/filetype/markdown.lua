-- Prevent tabs from being changed to spaces
vim.api.nvim_set_var("markdown_recommended_style", 0)

-- Commands to take place after buffer filetype is set
vim.api.nvim_create_autocmd(
	"FileType", {
		pattern = "markdown",
		callback = function()
			-- For compiling markdown to a pdf
			vim.keymap.set("n", "<leader>c", "<cmd>!pandoc -s % -o file.pdf<CR><CR>", { silent = true })
		end
	}
)
