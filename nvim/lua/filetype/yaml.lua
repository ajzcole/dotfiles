-- Prevent tabs from being changed to spaces
vim.api.nvim_set_var("yaml_recommended_style", 0)

-- Commands to take place after buffer filetype is set
vim.api.nvim_create_autocmd(
	"FileType", {
		pattern = "yaml",
		callback = function()
			-- Stop tabs from being expanded to spaces
			vim.opt.expandtab = false
		end
	}
)
