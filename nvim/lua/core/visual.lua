-- Colorscheme
vim.cmd("colorscheme colors")

-- Highlight the line that the cursor is on
vim.opt.cursorline = true

-- Relative line numbering
vim.opt.number = true
vim.opt.relativenumber = true

-- Fill characters for folds
vim.opt.fillchars = "fold: "
