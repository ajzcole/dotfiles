-- Leader
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Clear highlighting with escape
vim.keymap.set("n", "<esc>", "<cmd>noh<CR>", {silent = true})

-- Folding
vim.keymap.set("n", "<leader><leader>", "za")

-- Tree
vim.keymap.set("n", "<leader>e", "<cmd>NvimTreeToggle<CR>", {silent = true})

-- Switching windows with meta
vim.keymap.set("n", "<M-h>", "<C-w>h")
vim.keymap.set("n", "<M-j>", "<C-w>j")
vim.keymap.set("n", "<M-k>", "<C-w>k")
vim.keymap.set("n", "<M-l>", "<C-w>l")

-- Vimling plugin
vim.keymap.set("n", ",d", "<cmd>call ToggleDeadKeys()<CR>")
vim.keymap.set("n", ",i", "<cmd>call ToggleIPA()<CR>")
vim.keymap.set("n", ",p", "<cmd>call ToggleProse()<CR>")

-- LSP
vim.keymap.set("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", {silent = true})
vim.keymap.set("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", {silent = true})
vim.keymap.set("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", {silent = true})
vim.keymap.set("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", {silent = true})
