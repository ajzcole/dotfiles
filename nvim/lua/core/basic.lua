-- System clipboard
vim.api.nvim_set_option("clipboard", "unnamedplus")

-- Add angular brackets to matchpairs
vim.opt.matchpairs:append("<:>")

-- Disable autocommenting
vim.api.nvim_set_option("formatoptions", "1cjtq")

-- 8-char tabs
vim.opt.tabstop = 8
vim.opt.shiftwidth = 8
vim.opt.expandtab = false
vim.opt.softtabstop = 0

-- Disable autocommenting
vim.api.nvim_create_autocmd(
	"FileType", {
		pattern = "*",
		callback = function()
			vim.opt.formatoptions:remove("c")
			vim.opt.formatoptions:remove("r")
			vim.opt.formatoptions:remove("o")
		end
	}
)
