hi CursorLine		cterm=NONE			ctermbg=0

hi LineNr		cterm=NONE	ctermfg=7
hi CursorLineNr		cterm=bold	ctermfg=14
hi SignColumn						ctermbg=NONE

hi Comment		cterm=italic	ctermfg=7
hi Error		cterm=bold	ctermfg=9	ctermbg=NONE
hi Function		cterm=bold	ctermfg=14
hi Folded		cterm=bold	ctermfg=13	ctermbg=NONE
hi Operator		cterm=NONE	ctermfg=6
hi PreProc		cterm=bold	ctermfg=14
hi Statement		cterm=NONE	ctermfg=12
hi Type			cterm=NONE	ctermfg=3

hi spellBad		cterm=bold	ctermfg=9	ctermbg=NONE
hi spellCap		cterm=bold	ctermfg=12	ctermbg=NONE

hi shConditional	cterm=NONE	ctermfg=12
hi shQuote		cterm=NONE	ctermfg=12
hi shShellVariables	cterm=bold	ctermfg=14
hi shStatement		cterm=NONE	ctermfg=15
hi shVariable		cterm=bold	ctermfg=12

hi texCmdArgs		cterm=NONE	ctermfg=14
hi texCmdName		cterm=bold	ctermfg=12
hi texDef		cterm=NONE	ctermfg=12
hi texType		cterm=NONE	ctermfg=6

hi vimString		cterm=NONE	ctermfg=3
hi vimCommand		cterm=bold	ctermfg=14
hi vimOption		cterm=NONE	ctermfg=12
hi vimGroup		cterm=NONE	ctermfg=12

hi Pmenu				ctermfg=7	ctermbg=0

hi QuickScopePrimary	cterm=bold	ctermfg=6
hi QuickScopeSecondary	cterm=bold	ctermfg=7

hi Conceal						ctermbg=NONE
